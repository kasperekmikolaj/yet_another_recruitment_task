package mainPackage;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.FileTime;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

    private static int devMovedCounter = 0;
    private static int testMovedCounter = 0;
    private static String currentPath = null;
    private static String countersPath = null;
    private static boolean countersChanged = false;
    private static String thisFilename;

    public static void main(String[] args) {
        try {
            currentPath = new File(Main.class.getProtectionDomain().getCodeSource().getLocation().toURI()).getPath();
            thisFilename = currentPath.substring(currentPath.lastIndexOf("\\")+1);
            currentPath = currentPath.substring(0, currentPath.lastIndexOf("\\"));
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        if (currentPath == null) {
            System.out.println("Invalid path - program ended");
            return;
        }
        createAllDirectoriesAndFiles();

        while (true) {
            List<String> filenames = new ArrayList<>();
            try (Stream<Path> stream = Files.list(Paths.get(currentPath))) {
                filenames = stream
                        .filter(file -> !Files.isDirectory(file))
                        .map(Path::getFileName)
                        .map(Path::toString)
                        .collect(Collectors.toList());
            } catch (IOException e) {
                e.printStackTrace();
            }
            for (String filename : filenames) {
                if (!filename.equals(thisFilename)) {
                    moveFileToProperDir(filename);
                    if (countersChanged) {
                        updateCounters();
                    }
                }
            }
        } // end while loop
    }

    private static void updateCounters() {
        try (PrintWriter out = new PrintWriter(countersPath)) {
            out.println("moved files in total: " + Integer.toString(devMovedCounter + testMovedCounter));
            out.println("moved to DEV: " + Integer.toString(devMovedCounter));
            out.println("moved to TEST: " + Integer.toString(testMovedCounter));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        countersChanged = false;
    }

    private static void moveFileToProperDir(String filename) {
        String extension = filename.substring(filename.lastIndexOf(".") + 1);
        String fileToMovePath = currentPath + "\\" + filename;
        File fileToMove = new File(fileToMovePath);
        if (extension.equals("xml")) {
            fileToMove.renameTo(new File(currentPath + "\\" + "DEV\\" + filename));
            devMovedCounter += 1;
            countersChanged = true;
        } else if (extension.equals("jar")) {
            FileTime creationTime = null;
            try {
                creationTime = (FileTime) Files.getAttribute(Path.of(fileToMovePath), "creationTime");
            } catch (IOException e) {
                System.out.println("No creation time for file: " + fileToMovePath + " - nothing happened");
            }
            LocalDateTime fileLocalDateTimeCreation = LocalDateTime.ofInstant(creationTime.toInstant(), ZoneId.systemDefault());
            if (fileLocalDateTimeCreation.getHour() % 2 == 0) {
                fileToMove.renameTo(new File(currentPath + "\\" + "DEV\\" + filename));
                devMovedCounter += 1;
                countersChanged = true;
            } else {
                fileToMove.renameTo(new File(currentPath + "\\" + "TEST\\" + filename));
                testMovedCounter += 1;
                countersChanged = true;
            }
        }
    }

    private static void createAllDirectoriesAndFiles() {
        String homePath = currentPath + "/HOME";
        String devPath = currentPath + "/DEV";
        String testPath = currentPath + "/TEST";
        createDir(new File(homePath));
        createDir(new File(devPath));
        createDir(new File(testPath));
        countersPath = currentPath + "/HOME/counters.txt";
        try {
            new File(countersPath).createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void createDir(File file) {
        if (!file.exists()) {
            file.mkdirs();
        }
    }
}
