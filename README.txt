Program powinien:
stworzyć strukturę katalogów
HOME
DEV
TEST
W momencie pojawienia się w katalogu HOME pliku w zależności od rozszerzenia przeniesie go do folderu wg następujących reguł
plik z rozszerzeniem .jar, którego godzina utworzenia jest parzysta przenosimy do folderu DEV
plik z rozszerzeniem .jar, którego godzina utworzenia jest nieparzysta przenosimy do folderu TEST
plik z rozszerzeniem .xml, przenosimy do folderu DEV
Dodatkowo w nowo stworzonym pliku /home/count.txt należy przechowywać liczbę przeniesionych plików (wszystkich i w podziale na
katalogi), plik powinien w każdym momencie działania programu przechowywać aktualną liczbę przetworzonych plików.

To build:
open terminal in project folder and run:
mvn clean
mvn package
to run:
take file segregator-1.0-SNAPSHOT.jar from target folder and place in some testing dir with .jar and .xml files
to run program enter in terminal:
java -jar segregator-1.0-SNAPSHOT.jar